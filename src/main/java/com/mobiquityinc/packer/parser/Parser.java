package com.mobiquityinc.packer.parser;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.exceptions.APIException;


public interface Parser {
	public Collection<Package> parseFile(String filePath) throws APIException;
}
