package com.mobiquityinc.packer.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;




@Configuration
public class PackageConstraintConfig {
	
	

	Logger logger=LoggerFactory.getLogger(PackageConstraintConfig.class);
	
	
	@Value("${maxWeight}")
	private int maxWeight;
	
	@Value("${product.maxWeight}")
	private int productMaxWeight;

	@Value("${product.maxAmount}")
	private int productMaxAmount;
	
	private static int maxWeightStatic;
	private static int productMaxWeightStatic;
	private static int productMaxAmountStatic;

		
	@PostConstruct
	public void init() {
		maxWeightStatic=maxWeight;
		productMaxWeightStatic=productMaxWeight;
		productMaxAmountStatic=productMaxAmount;
	}
	
	
	
	public enum Costraint {
		MAX_WEIGHT(maxWeightStatic), 
		MAX_WEIGHT_PRODUCT(productMaxWeightStatic), 
		MAX_AMOUNT_PRODUCT(productMaxAmountStatic);

		private int val;

		Costraint(int val) {
			this.val = val;
		}

		public int getVal() {
			return val;
		}
		

	}
	
}
