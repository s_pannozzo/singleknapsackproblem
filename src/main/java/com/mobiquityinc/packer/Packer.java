package com.mobiquityinc.packer;


import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.entity.Solution;
import com.mobiquityinc.packer.exceptions.APIException;
import com.mobiquityinc.packer.parser.Parser;
import com.mobiquityinc.packer.processor.Processor;

@Component
public class Packer {

	static Logger logger=LoggerFactory.getLogger(Packer.class);
	
	
	private static Parser staticParser;
	private static Processor staticProcessor;

	public Packer() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	Parser parser;
	
	@Autowired
	Processor packageProcessor;
	
	@PostConstruct
	public void initStaticParser() {
		staticParser=parser;
		staticProcessor=packageProcessor;
	}
	
	
	
	public static void pack(String filePath) throws APIException {
		
		Collection<Package> inputToCheck=staticParser.parseFile(filePath);
		
		Collection<Solution> totalSolutions=new ArrayList<Solution>();
		
		inputToCheck.forEach(p->{
			Solution s=staticProcessor.process(p);
			totalSolutions.add(s);
		});
//		logger.info(totalSolutions.toString());
		for (Solution solution : totalSolutions) {
			logger.info(solution.getSolutionProductsIds().toString());
			logger.info("-");
		}
	}

	

}
