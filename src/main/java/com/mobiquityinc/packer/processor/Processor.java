package com.mobiquityinc.packer.processor;

import java.util.List;

import org.springframework.stereotype.Service;
import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.entity.Product;
import com.mobiquityinc.packer.entity.Solution;


public interface Processor {
	public Solution process(Package p) ;

}
