package com.mobiquityinc.packer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mobiquityinc.packer.config.PackageConstraintConfig;
import com.mobiquityinc.packer.entity.Product;
import com.mobiquityinc.packer.entity.Package;

@ActiveProfiles("constraints")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ConstraintsTest {
	
	
	Logger logger = LoggerFactory.getLogger(ConstraintsTest.class);

	@Value("${maxWeight}")
	private int maxWeight;
	
	@Value("${product.maxWeight}")
	private int productMaxWeight;

	@Value("${product.maxAmount}")
	private int productMaxAmount;
	
	@Autowired
	List<Product> products;
	
	@Test
	public void A_check_constraints_values(){
		assertEquals(maxWeight, PackageConstraintConfig.Costraint.MAX_WEIGHT.getVal());
		assertEquals(productMaxWeight, PackageConstraintConfig.Costraint.MAX_WEIGHT_PRODUCT.getVal());
		assertEquals(productMaxAmount, PackageConstraintConfig.Costraint.MAX_AMOUNT_PRODUCT.getVal());
	}

	@Test
	public void B_check_max_weight_limit_in_package(){
		
		double tooMuchWeight= PackageConstraintConfig.Costraint.MAX_WEIGHT.getVal()+1;
		
		Package package1=new Package(tooMuchWeight, products);
		
		assertTrue(package1.getWeight()<tooMuchWeight);
		assertTrue(PackageConstraintConfig.Costraint.MAX_WEIGHT.getVal() == package1.getWeight());
	}
	@Test
	public void C_check_max_weight_product_limit_in_package_products(){
		int tooMuchWeightProductCounter= 0;
		double tooMuchProductWeight=PackageConstraintConfig.Costraint.MAX_WEIGHT_PRODUCT.getVal()+1;
		
		int maxProductAmount=PackageConstraintConfig.Costraint.MAX_AMOUNT_PRODUCT.getVal();
		
		for (Product p : products) {
			if (p.getWeight()>tooMuchProductWeight) {
				tooMuchWeightProductCounter++;
			}
		}			
			
		Package package1=new Package(PackageConstraintConfig.Costraint.MAX_WEIGHT.getVal(), products);
		
		int x=Math.abs(tooMuchWeightProductCounter-maxProductAmount);
		
		
		assertTrue(package1.getProducts().size()<products.size());
		assertEquals(products.size()-tooMuchWeightProductCounter-x, package1.getProducts().size());
	}
	@Test
	public void D_check_max_amount_product_limit_in_package_products(){
		
		int maxProductAmount=PackageConstraintConfig.Costraint.MAX_AMOUNT_PRODUCT.getVal();
		
		Package package1=new Package(PackageConstraintConfig.Costraint.MAX_WEIGHT.getVal(), products);
		assertEquals(maxProductAmount, package1.getProducts().size());

		
	}
}
