package com.mobiquityinc.packer.config;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.mobiquityinc.packer.entity.Product;
import com.mobiquityinc.packer.exceptions.APIException;

@Profile("constraints")
@Component
@Configuration
public class ProductsConfig {
	
	private static final Type REVIEW_TYPE = new TypeToken<List<Product>>() {}.getType();

	Logger logger=LoggerFactory.getLogger(ProductsConfig.class);
	
	@Value("classpath:../classes/data/json/products.json")
	private Resource productsFiles;

	JsonReader reader ;
	
	private Gson gson=new Gson();
	
	@Bean(name = "products")
	public List<Product> getProducts() {
		List<Product> data = gson.fromJson(reader, REVIEW_TYPE); 
		return data;
	}
		
	@PostConstruct
	public void init() throws APIException, IOException{
		reader= new JsonReader(new FileReader(productsFiles.getFile()));
	}
	
}
