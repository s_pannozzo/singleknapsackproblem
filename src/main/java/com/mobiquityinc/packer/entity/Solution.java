package com.mobiquityinc.packer.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


public class Solution {
	Collection<Product> products;
	
	double cost;
	double weight;

	@SerializedName("package") 
	private Package p;

	
	public Package getPackage() {
		return p;
	}

	public Collection<Product> getProducts() {
		return products;
	}

	public double getCost() {
		return cost;
	}
	
	
	
	public double getWeight() {
		return weight;
	}

	public Solution(Package p, Collection<Product> products, double cost) {
		super();
		this.p=p;
		this.products = products;
		this.cost = cost;
		
		this.weight=products.stream().mapToDouble(Product::getWeight).sum();
		
	}
	

	public Collection<Integer> getSolutionProductsIds(){
		
		List<Integer> x=new ArrayList<Integer>();
		
		return products.stream().map(p->Integer.parseInt(p.getId())).collect(Collectors.toList());
		
	}
	
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
