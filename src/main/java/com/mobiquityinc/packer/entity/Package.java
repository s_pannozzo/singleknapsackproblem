package com.mobiquityinc.packer.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.mobiquityinc.packer.config.PackageConstraintConfig;

public class Package implements Cloneable {
	private double weight;

	Collection<Product> products;

	public double getWeight() {
		return weight;
	}

	public Collection<Product> getProducts() {
		return products;
	}

	public Package(double weight, Collection<Product> products) {
		super();

		double maxWeight = PackageConstraintConfig.Costraint.MAX_WEIGHT.getVal();

		checkMaxPackageWeight(weight, maxWeight);

		checkProductsWithMaxProductWeightConstraint(products);

		checkMaxProductsAmount(products);
	}

	private void checkMaxPackageWeight(double weight, double maxWeight) {
		if (weight <= maxWeight) {
			this.weight = weight;
		} else {
			this.weight = maxWeight;
		}
	}

	private void checkProductsWithMaxProductWeightConstraint(Collection<Product> products) {
		double maxProductWeight = PackageConstraintConfig.Costraint.MAX_WEIGHT_PRODUCT.getVal();

		Collection<Product> productX = new ArrayList<Product>(products);

		productX.removeIf(p -> {
			return p.getWeight() > maxProductWeight;
		});

		this.products = productX;
	}

	private void checkMaxProductsAmount(Collection<Product> products) {
		int maxProductAmount = PackageConstraintConfig.Costraint.MAX_AMOUNT_PRODUCT.getVal();

		List<Product> listProducts = new ArrayList<Product>(products);

		if (listProducts.size() > maxProductAmount) {
			listProducts = listProducts.subList(0, maxProductAmount);
		}
		this.products = listProducts;
	}

	public Package normalizeProducts() {

		Package x = null;

		products.removeIf(p -> {
			return p.getWeight() > weight;
		});

		List<Product> xProduct = new ArrayList<Product>(products);

		x = new Package(this.weight, xProduct);

		return x;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}

	@Override
	public Package clone() throws CloneNotSupportedException {
		Package x = null;

		Collection<Product> ps = new ArrayList<Product>();

		for (Product p : products) {
			ps.add(p.clone());
		}

		x = new Package(weight, ps);

		return x;
	}
}
