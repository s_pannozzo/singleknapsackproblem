package com.mobiquityinc.packer.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mobiquityinc.packer.exceptions.APIException;
import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.entity.Product;
import com.mobiquityinc.packer.entity.Solution;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class PackageProcessorTest extends PackageProcessor {

	Logger logger = LoggerFactory.getLogger(PackageProcessorTest.class);

	@Value("classpath:../classes/inputFile.txt")
	private Resource inputFile;

	@Autowired
	Processor processor;

	private static Product[] producstArray;

	private double w=14;

	

	private static int maxIndex;

	private static Product p1;
	private static Product p2;
	private static Product p3;
	
	private static Collection<Product> products ;
	
	private static Package packageX;
	
//	private static List<List<Product>> solutions;

	@Test
	public void A_check_items_order() throws APIException, IOException {
		p1 = new Product("1", 5, 20);
		p2 = new Product("2", 10, 30);
		p3 = new Product("3", 3, 9);

		products=new ArrayList<Product>();
		
		products.add(p1);
		products.add(p2);
		products.add(p3);


		producstArray = products.toArray(new Product[products.size()]);
		assertNotNull(producstArray);

		Collection<Product> x = Arrays.asList(producstArray);

		Arrays.sort(producstArray);

		x = Arrays.asList(producstArray);
		
		assertEquals(p2, producstArray[0]);

	}

	@Test
	public void B_check_cost_on_weight() throws APIException, IOException {
	
		maxIndex=gexMacConWIndex(products);
		
		assertEquals(1, maxIndex);
		assertEquals(p1, producstArray[maxIndex]);

	}

	@Test
	public void C_iterate_on_products_till_have_max_weigth() throws APIException, IOException {
	
		int startingIndex=maxIndex;
		
	}
	
	@Test
	public void D_check_solutions() throws APIException, IOException {
		
			
		packageX=new Package(w,products);
		
		Solution solution=processor.process(packageX);
		
		assertNotNull(solution);
		
		assertEquals("39.0", ""+solution.getCost());
		assertEquals(2, solution.getProducts().size());
		
		
		
		
	}
	

}
