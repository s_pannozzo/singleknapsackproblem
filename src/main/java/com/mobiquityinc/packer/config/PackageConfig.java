package com.mobiquityinc.packer.config;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.mobiquityinc.packer.Packer;
import com.mobiquityinc.packer.exceptions.APIException;

@Profile("default")
@Configuration

public class PackageConfig {
	
	Logger logger=LoggerFactory.getLogger(PackageConfig.class);
	
	@Value("classpath:../classes/data/inputFile.txt")
	private Resource inputFile;
	
		
	@PostConstruct
	public void init() throws APIException, IOException{
		Packer.pack(inputFile.getFile().getPath());		
	}
	
}
