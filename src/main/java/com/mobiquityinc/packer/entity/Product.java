package com.mobiquityinc.packer.entity;

import com.google.gson.Gson;
import com.mobiquityinc.packer.config.PackageConstraintConfig;

public class Product implements Comparable<Product>,Cloneable {
	private String id;
	private double weight;
	private double cost;
	
	public String getId() {
		return id;
	}
	public double getWeight() {
		return weight;
	}
	public double getCost() {
		return cost;
	}
	public Product(String id, double weight, double cost) {
		super();
		this.id = id;
				
		this.weight = weight;
		this.cost = cost;
	}
	
	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	@Override
	public int hashCode() {
		
		return 0;
	}
	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof Product) {
			Product p=(Product)obj;
						
			if (p.getId().equals(id)) {
				return true;
			}
		}
		return false;
	}
	@Override
	public int compareTo(Product p) {
		if (weight>p.getWeight()) {
			return -1;
		}
		if (weight<p.getWeight()) {
			return 1;
		}
		return 0;
	}
	@Override
	protected Product clone() throws CloneNotSupportedException {
		Product x=new Product(id, weight, cost);
		
		return x;
	}
}
