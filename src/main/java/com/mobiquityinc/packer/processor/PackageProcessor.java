package com.mobiquityinc.packer.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.entity.Product;
import com.mobiquityinc.packer.entity.Solution;

@Service
public class PackageProcessor implements Processor {

	Logger logger = LoggerFactory.getLogger(PackageProcessor.class);

	private List<List<Product>>  solutions=new ArrayList<List<Product>>();
	
	
	double weight=0;
	double cost=0;
	
	@Override
	public Solution process(Package p) {
		solutions.clear();
		Collection<Product> products = p.getProducts();

		Product[] producstArray=products.toArray(new Product[products.size()]);
		Arrays.sort(producstArray);
		
		int startingIndex=0;
		
		if (producstArray.length==0) {
			return new Solution(p,products, 0d);
		}
		if (producstArray.length>1) {
			startingIndex=gexMacConWIndex(products);
		}
		
		
		while (startingIndex>=0) {
			
			Product px=producstArray[startingIndex];
			Package x = null;
			try {
				x = p.clone();
				
				x=x.normalizeProducts();
				
				if (x.getProducts().size()==0) {
					break;
				}
				
			} catch (CloneNotSupportedException e) {
				logger.error(e.getMessage(),e);
			}
			
			Collection<Product> solution=getSolution(x,px,startingIndex);
			

			
			solutions.add(solution.stream().collect(Collectors.toList()));
			startingIndex--;
			
		}
		Solution solution=new Solution(p,Collections.EMPTY_LIST,0d);
		if (solutions.size()>0) {
			SortedMap<Double, List<Product>> mappedSolution=mapSolution(solutions);
			
			Double totalCost=mappedSolution.lastKey();
			
			solution=new Solution(p,mappedSolution.get(totalCost), totalCost);
			
		}
		
		
		return solution;
		
	}


	private Collection<Product> getSolution(Package x, Product px, int startingIndex2) {
		Collection<Product> products=new HashSet<Product>();
		int startingIndex=0;
		if (px==null) {
			
			Product[] producstArray=x.getProducts().toArray(new Product[products.size()]);
			Arrays.sort(producstArray);
			
			if (producstArray.length>1) {
				startingIndex=gexMacConWIndex(products);
			}
			px=producstArray[startingIndex];
		}
		while (startingIndex>=0) {
			
			products.add(px);
			
			double weight=x.getWeight()-px.getWeight();
			x.getProducts().remove(px);
			if (weight>0) {
				Package newPackage=new Package(weight,new ArrayList<Product>(x.getProducts()));
				
				newPackage=newPackage.normalizeProducts();
				
								
				if (!newPackage.getProducts().isEmpty()) {
					products.addAll(getSolution(newPackage,null,--startingIndex));
				}else {
					break;
				}
			}else {
				break;
			}
		}
		
		
		return products;
	}


	protected int gexMacConWIndex(Collection<Product> products) {
		Product[] producstArray=products.toArray(new Product[products.size()]);
		
		Arrays.sort(producstArray);
		
		int maxIndex=0;
		double maxCW=0;
		
		for (int i = 0; i < producstArray.length; i++) {
			Product product=producstArray[i];
			
			double costOnweight=product.getCost()/product.getWeight();
			
			if (costOnweight>maxCW) {
				maxCW=costOnweight;
				maxIndex=i;
			}	
		}
		return maxIndex;
	}


	
//	protected List<List<Product>> getSolutions() {
//		return solutions;
//	}
	
	protected SortedMap<Double, List<Product>> mapSolution(List<List<Product>> solutions) {
		
		SortedMap<Double, List<Product>> m=new TreeMap<Double, List<Product>>();
		
		for (List<Product> solution : solutions) {
			Double totalCost=solution.stream().mapToDouble(Product::getCost).sum();
			m.put(totalCost, solution);
		}
		
		return m;
	}

}
