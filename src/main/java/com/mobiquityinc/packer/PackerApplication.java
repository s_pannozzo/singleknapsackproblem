package com.mobiquityinc.packer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mobiquityinc.packer.config.PackageConfig;
import com.mobiquityinc.packer.config.PackageConstraintConfig;

@SpringBootApplication
@EnableAutoConfiguration
public class PackerApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(new Class[]{PackerApplication.class, Packer.class, PackageConstraintConfig.class, PackageConfig.class}, args);

	}

	
	

}
