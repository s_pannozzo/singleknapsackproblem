package com.mobiquityinc.packer;

import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mobiquityinc.packer.exceptions.APIException;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class PackerApplicationTests {

	Logger logger = LoggerFactory.getLogger(PackerApplicationTests.class);

	@Value("classpath:../classes/data/inputFile.txt")
	private Resource inputFile;
	
	@Value("classpath:../classes/data/myFile.txt")
	private Resource myCases;
	
	@Autowired
	Packer packer;
	
	@Test
	public void A_pack_OK() throws APIException, IOException {
		Packer.pack(inputFile.getFile().getPath());		
	}

	@Test(expected =APIException.class )
	public void B_pack_KO() throws APIException {
		Packer.pack("c:/not_existing_file.txt");		
	}
	@Test
	public void C_check_pack_algorithm_my_cases() throws APIException, IOException {
		Packer.pack(myCases.getFile().getPath());		
	}
	
}
