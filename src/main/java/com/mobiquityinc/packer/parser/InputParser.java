package com.mobiquityinc.packer.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.entity.Product;
import com.mobiquityinc.packer.exceptions.APIException;

@Service
public class InputParser implements Parser{
	
	
	public InputParser() {
		// TODO Auto-generated constructor stub
	}
	
	Logger logger = LoggerFactory.getLogger(InputParser.class);
	
	public Collection<Package> parseFile(String filePath) throws APIException {

		Collection<Package> returnValue = new ArrayList<Package>();
		BufferedReader reader=null;
		try {
			reader= Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8);
			
		} catch (Exception e) {
			throw new APIException();
		}

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				
				Package p=mapTextLine(line);
				
				returnValue.add(p);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
		
		return returnValue;
	}

	private Package mapTextLine(String line) {
		Package packageX=null;
		
		String[] splittedString=line.split(":");
		
		double weight=Double.parseDouble(splittedString[0]);
		
		splittedString=splittedString[1].split(" ");
		
		HashSet<Product> products=new HashSet<Product>();
		
		for (int i = 1; i < splittedString.length; i++) {
			
			Product p=null;
			try {
				String str=splittedString[i];
				str=str.substring(str.indexOf("(")+1,str.indexOf(")"));
				
				String[] productValues=str.split(",");
				
				String id=productValues[0];
				double pWeight=Double.parseDouble(productValues[1]);
				double cost=Double.parseDouble(productValues[2].replace("€", ""));
				
				p=new Product(id, pWeight, cost);
				
				products.add(p);
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
			
			
			
		}
		packageX=new Package(weight, products);
		return packageX;
	}
}
