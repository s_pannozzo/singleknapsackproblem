package com.mobiquityinc.packer.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mobiquityinc.packer.entity.Package;
import com.mobiquityinc.packer.exceptions.APIException;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class InputParserTest {

	Logger logger = LoggerFactory.getLogger(InputParserTest.class);

	@Value("classpath:../classes/data/inputFile.txt")
	private Resource inputFile;
	
	@Autowired
	InputParser parser;
	
	@Test
	public void A_check_if_input_file_returns_items() throws APIException, IOException {
		Collection<Package> items=parser.parseFile(inputFile.getFile().getPath());
		
		assertNotNull(items);
		assertTrue(items.size()>0);
		
		
		List<Package> packageItems=items.stream().collect(Collectors.toList());
		
		Package package1=packageItems.get(0);
		assertNotNull(package1);
		assertEquals("81.0", ""+package1.getWeight());
		
		package1=package1.normalizeProducts();
		assertEquals(5, package1.getProducts().size());
		
		Package package2=packageItems.get(1);
		assertNotNull(package2);
		
		assertEquals("8.0", ""+package2.getWeight());
		package2=package2.normalizeProducts();
		assertEquals(0, package2.getProducts().size());
	}

}
